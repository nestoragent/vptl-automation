package com.vptl.pages;

import com.vptl.lib.Init;
import com.vptl.lib.Page;
import com.vptl.lib.pageFactory.PageEntry;
import org.openqa.selenium.By;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
@PageEntry(title = "Home")
public class HomePage extends Page {

    public HomePage() {
        waitForVisibilityOf(By.xpath("//android.widget.TextView[@content-desc='Play Маркет']"));
    }

    public void open_play_market() {
        Init.getDriver().findElement(By.xpath("//android.widget.TextView[@content-desc='Play Маркет']")).click();
    }

}
