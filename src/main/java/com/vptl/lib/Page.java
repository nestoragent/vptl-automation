package com.vptl.lib;

import com.vptl.lib.pageFactory.PageEntry;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class Page {
    
    protected void waitForVisibilityOf(By locator) {
        List<WebElement> elements = Init.getDriver().findElements(locator);
        int i = 0;
        boolean isVisible = false;
        while (i < 10
                && elements.size() < 0
                && !isVisible) {
            System.out.println("work!!! + " + i);
            try {
                Thread.sleep(2 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            elements = Init.getDriver().findElements(locator);
            if (elements.size() > 0 && elements.get(0).isDisplayed()) {
                System.out.println("work displayed = " + i);
                isVisible = true;
            }
            i++;
        }
//        WebDriverWait wait = new WebDriverWait(Init.getDriver(), 60);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForClickabilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(Init.getDriver(), 60);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void scrollPageDown() {
        JavascriptExecutor js = (JavascriptExecutor) Init.getDriver();
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("direction", "down");
        js.executeScript("mobile: scroll", scrollObject);
    }

    public void scrollPageUp() {
        JavascriptExecutor js = (JavascriptExecutor) Init.getDriver();
        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
        swipeObject.put("startX", 0.50);
        swipeObject.put("startY", 0.95);
        swipeObject.put("endX", 0.50);
        swipeObject.put("endY", 0.01);
        swipeObject.put("duration", 3.0);
        js.executeScript("mobile: swipe", swipeObject);
    }


    public void swipeLeftToRight() {
        JavascriptExecutor js = (JavascriptExecutor) Init.getDriver();
        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
        swipeObject.put("startX", 0.01);
        swipeObject.put("startY", 0.5);
        swipeObject.put("endX", 0.9);
        swipeObject.put("endY", 0.6);
        swipeObject.put("duration", 3.0);
        js.executeScript("mobile: swipe", swipeObject);
    }

    public void swipeRightToLeft() {
        JavascriptExecutor js = (JavascriptExecutor) Init.getDriver();
        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
        swipeObject.put("startX", 0.9);
        swipeObject.put("startY", 0.5);
        swipeObject.put("endX", 0.01);
        swipeObject.put("endY", 0.5);
        swipeObject.put("duration", 3.0);
        js.executeScript("mobile: swipe", swipeObject);
    }

    public void swipeFirstCarouselFromRightToLeft() {
        JavascriptExecutor js = (JavascriptExecutor) Init.getDriver();
        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
        swipeObject.put("startX", 0.9);
        swipeObject.put("startY", 0.2);
        swipeObject.put("endX", 0.01);
        swipeObject.put("endY", 0.2);
        swipeObject.put("duration", 3.0);
        js.executeScript("mobile: swipe", swipeObject);
    }

    public void performTapAction(WebElement elementToTap) {
        JavascriptExecutor js = (JavascriptExecutor) Init.getDriver();
        HashMap<String, Double> tapObject = new HashMap<String, Double>();
        tapObject.put("x", (double) 360); // in pixels from left
        tapObject.put("y", (double) 170); // in pixels from top
        tapObject.put("element", Double.valueOf(((RemoteWebElement) elementToTap).getId()));
        js.executeScript("mobile: tap", tapObject);
    }



    /**
     * Return a static text locator by exact text *
     */
    public static By for_text_exact(String text) {
        return By.xpath("//android.widget.TextView[@text='" + text + "']");
    }

    public static By for_find(String value) {
        return By.xpath("//*[@content-desc=\"" + value + "\" or @resource-id=\"" + value +
                "\" or @text=\"" + value + "\"] | //*[contains(translate(@content-desc,\"" + value +
                "\",\"" + value + "\"), \"" + value + "\") or contains(translate(@text,\"" + value +
                "\",\"" + value + "\"), \"" + value + "\") or @resource-id=\"" + value + "\"]");
    }

//    public static WebElement find(String value) {
//        return element(for_find(value));
//    }

    /**
     * Wait 30 seconds for locator to find an element *
     */
    public static WebElement wait(By locator) {
        return new WebDriverWait(Init.getDriver(), 60).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    /**
     * Wait 60 seconds for locator to find all elements *
     */
    public static List<WebElement> waitAll(By locator) {
        return new WebDriverWait(Init.getDriver(), 60).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    /**
     * Wait 60 seconds for locator to not find a visible element *
     */
    public static boolean waitInvisible(By locator) {
        return new WebDriverWait(Init.getDriver(), 60).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    /**
     * <p>
     * getTitle.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTitle() {
        return this.getClass().getAnnotation(PageEntry.class).title();
    }

    public void takeAction(String action, Object... param) throws Throwable {
        action = action.replaceAll(" ", "_");
        try {
            MethodUtils.invokeMethod(this, action, param);
        } catch (NoSuchMethodException e) {
            StringBuilder sb = new StringBuilder();

            sb.append("There is no \"").append(action).append("\" action ")
                    .append("in ")
                    .append(this.getTitle()).append(" page object")
                    .append("\n");
            sb.append("Possible actions are:")
                    .append("\n");
            Class tClass = this.getClass();
            Method[] methods = tClass.getDeclaredMethods();
            for (Method method : methods) {
                sb.append("\t\"")
                        .append(this.getTitle()).append("\"->\"")
                        .append(method.getName()).append("\" with ")
                        .append(method.getGenericParameterTypes().length)
                        .append(" input parameters").append("\n");
            }
            throw new NoSuchMethodException(sb.toString());
        } catch (InvocationTargetException ex) {
            throw ex.getCause();
        }
    }

    public void closeNotificationsRate () {
        try {
            Thread.sleep(2 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (Init.getDriver().findElements(By.xpath("//android.widget.TextView[@text='Rate Instagram']")).size() > 0)
            Init.getDriver().findElement(By.xpath("//android.widget.TextView[@text='No, Thanks']")).click();
    }
}
