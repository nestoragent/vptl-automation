package com.vptl.lib;

import com.vptl.lib.pageFactory.PageFactory;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class Init {
    protected static WebDriver driver;
    private static PageFactory pageFactory;

    public static WebDriver getDriver() {
        if (null == driver) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("device","Android");
            capabilities.setCapability("deviceName","Android");
            capabilities.setCapability("platformName","Android");
            capabilities.setCapability(MobileCapabilityType.APP_PACKAGE, "com.android.vending");
            capabilities.setCapability(MobileCapabilityType.APP_ACTIVITY, ".AssetBrowserActivity");
            capabilities.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, ".AssetBrowserActivity");
            capabilities.setCapability(MobileCapabilityType.DEVICE_READY_TIMEOUT, 40);
            capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 180);
            try {
                driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
            } catch (MalformedURLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return driver;
    }

    public static PageFactory getPageFactory() {
        if (null == pageFactory) {
            pageFactory = new PageFactory(Props.get("automation.pages"));
        }
        return pageFactory;
    }
}
