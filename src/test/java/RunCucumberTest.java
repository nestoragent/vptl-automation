
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue = {"com.vptl.stepDefinitions"},
		features = {"src/test/resources/features/"},
		tags = {"@post"},
		plugin = {"pretty", "html:target/cucumber-html-report"}
		)
public class RunCucumberTest {}